package am.egs.spring.controller;

import am.egs.spring.beans.UserAccountBean;

/**
 * Created by haykh on 4/29/2019.
 */

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.WebApplicationContext;


@Controller
public class LoginController {

  /* Autowired the web application context object. */
  @Autowired
  private WebApplicationContext webContext;

  public static final String STATUS_MESSAGE = "STATUS_MESSAGE";

  // This method map to http://localhost:8080/SpringMVCByAnnotation/showLogin.html
  @RequestMapping("/showLogin.html")
  public String showLoginPage() {
    return "show_login";
  }

  // This method map to doLogin.html
  @RequestMapping("/doLogin.html")
  public String doLogin(Model model, @ModelAttribute("userName")
      String userName,
      @ModelAttribute("password")
          String password,
      HttpServletRequest req) {

    System.out.println("userName = " + userName);

    System.out.println("password = " + password);

    // Get spring bean from the autowired web application context.
    UserAccountBean userAccountBean = (UserAccountBean)webContext.getBean("userAccountBean");

    boolean checkResult = userAccountBean.checkUserLogin(userName, password);

    if(checkResult)
    {
      model.addAttribute(STATUS_MESSAGE, "User account is correct. ");
      return "login_success";
    }else
    {
      model.addAttribute(STATUS_MESSAGE, "User account is not correct. ");
      return "show_login";
    }
  }

}
